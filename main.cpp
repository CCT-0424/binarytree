#include <iostream>

using namespace std;

//Definição do nó da árvore binária
struct node {
    int value;
    node *left; //nó à esquerda
    node *right; //nó à direita
};

//Raiz da árvore
node *root = NULL;

void inserir(int value, node *leaf){
    if (value < leaf->value){
        if (leaf->left != nullptr){
            inserir(value, leaf->left);
        } else {
            leaf->left = new node;
            leaf->left->value = value;
            leaf->left->left = nullptr;
            leaf->left->right = nullptr;
        }
    } else
    if (value >= leaf->value){
        if (leaf->right != nullptr){
            inserir(value, leaf->right);
        } else {
            leaf->right = new node;
            leaf->right->value = value;
            leaf->right->left = nullptr;
            leaf->right->right = nullptr;
        }
    }
}

//Chamar sempre por esta funçao!!
void inserir(int value){
    if (root != nullptr){
        inserir(value,root);
    } else {
        root = new node;
        root->value = value;
        root->left = nullptr;
        root->right = nullptr;
    }
}

//pesquisa um valor na árvore binária!!
node *pesquisar(int value, node *leaf){
    if (leaf != nullptr){
        if (value == leaf->value){
            return leaf;
        }
        if (value < leaf->value){
            return pesquisar(value,leaf->left);
        } else {
            return pesquisar(value,leaf->right);
        }
    } else
        return nullptr;
}

//In order printing
void inOrder(node *no){
    if (no == nullptr)
        return;
    inOrder(no->left);
    cout << no->value <<" ";
    inOrder(no->right);
}

//Pre order printing
void preOrder(node *no){
    if (no == nullptr)
        return;
    cout << no->value <<" ";
    preOrder(no->left);
    preOrder(no->right);
}

//Post order printing
void postOrder(node *no){
    if (no == nullptr)
        return;
    postOrder(no->left);
    postOrder(no->right);
    cout << no->value <<" ";
}

//usar esta função aqui para buscar um valor!!
node *pesquisar(int value){
    return pesquisar(value,root);
}

int main()
{
    inserir(5);
    inserir(4);
    inserir(10);
    inserir(2);
    inserir(8);
    inserir(7);
    inserir(6);
    node* res = pesquisar(2);
    if (res != nullptr){
        cout << "achou o valor: " << res->value << endl;
    } else {
        cout << "Não foi possível achar o valor!!" << endl;
    }
    cout << endl << "Imprimindo pre order" << endl;
    preOrder(root);
    cout << endl << "Imprimindo in order" << endl;
    inOrder(root);
    cout << endl << "Imprimindo post order" << endl;
    postOrder(root);

    return 0;
}
